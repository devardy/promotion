<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Message
{

    public function custom_error_msg($url, $message)
    {
        $type = 'error';
        set_message($type, $message);
        redirect($url);
    }

    public function custom_success_msg($url, $message)
    {
        $type = 'success';
        set_message($type, $message);
        redirect($url);
    }

    public function save_success($url)
    {
        $type = 'success';
        $message = 'save_success';
        set_message($type, $message);
        redirect($url);
    }

    public function delete_success($url)
    {
        $type = 'success';
        $message = 'delete_success';
        set_message($type, $message);
        redirect($url);
    }

    public function norecord_found($url)
    {
        $type = 'error';
        $message = 'no_record_found';
        set_message($type, $message);
        redirect($url);
    }

    public function success_msg()
    {
        return $message = 'save_success';
    }

    public function delete_msg()
    {
        return $message = 'delete_success';
    }
}
