 <div id="slider">
     <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <div class="carousel-inner">
             <?php
                $count = 0;
                $indicators = '';
                foreach ($slider as $row) :
                    $count++;
                    if ($count === 1) {
                        $class = 'active';
                    } else {
                        $class = '';
                    }
                ?>
                 <div class="carousel-item <?php echo $class; ?>">
                     <img src="<?= base_url() . 'assets/uploads/slider/' . $row->image ?>" class="slider-img" width="100%">
                 </div>

                 <?php $indicators .= '<li data-target="#carouselExampleIndicators" data-slide-to="' . $count . '" class="' . $class . '"></li>' ?>
             <?php endforeach; ?>
             <ol class="carousel-indicators">
                 <?= $indicators; ?>
             </ol>
         </div>
         <a class=" carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
             <span class="carousel-control-next-icon" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
         </a>
     </div>
 </div>
 <div id="main">
     <div class="container">
         <div class="row">
             <?php foreach ($category as $item) : ?>
                 <div class="col-md-3">
                     <div class="promotion">
                         <a href="<?= base_url('home/details/' . $item->id) ?>">
                             <div class="promo-img">
                                 <img src="<?= base_url(PROMOTION_IMAGE . $item->image) ?>" alt="">
                             </div>
                             <div class="promo-body">
                                 <h2 class="promo-name"><?= $item->title ?></h2>
                                 <p class="promo-text"><?= $item->slug ?>
                                 </p>
                                 <div class="promo-bar"></div>
                                 <h2 class="promo-price"><?= $item->price ?></h2>
                             </div>
                         </a>
                     </div>
                 </div>
             <?php endforeach; ?>
         </div>
     </div>
     <div class="loading" style="display: none;">
         <div class="content"><img src="<?php echo base_url() . 'assets/img/loading.gif'; ?>" /></div>
     </div>
 </div>