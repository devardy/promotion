<div class="container">
    <?php foreach ($list as $item) : ?>
        <div class="row" id="details">

            <div class="col-md-10 offset-1">
                <div class="banner-lv">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="detail-title"><?= $item->slug ?></h3>
                            <img src="<?= base_url() . 'assets/uploads/promotion/' . $item->image; ?>" width="520" height="343" alt="">
                        </div>
                        <div class="col-md-4">
                            <div class="price">
                                <p class="p-title"><?= $item->price ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10 offset-1">
                <div class="banner-lv">
                    <?= $item->content ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>