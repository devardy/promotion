 <div id="slider">
     <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
         <div class="carousel-inner">
             <?php
                $count = 0;
                $indicators = '';
                foreach ($slider as $row) :
                    $count++;
                    if ($count === 1) {
                        $class = 'active';
                    } else {
                        $class = '';
                    }
                ?>
                 <div class="carousel-item <?php echo $class; ?>">
                     <img src="<?= base_url() . 'assets/uploads/slider/' . $row->image ?>" class="slider-img" width="100%">
                 </div>

                 <?php $indicators .= '<li data-target="#carouselExampleIndicators" data-slide-to="' . $count . '" class="' . $class . '"></li>' ?>
             <?php endforeach; ?>
             <ol class="carousel-indicators">
                 <?= $indicators; ?>
             </ol>
         </div>
         <a class=" carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
         </a>
         <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
             <span class="carousel-control-next-icon" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
         </a>
     </div>
 </div>
 <div id="main">
     <div class="container">
         <div class="row" id="promotion">
             <!-- <div class="col-md-3">
                 <div class="promotion">
                     <a href="#">
                         <div class="promo-img">
                             <img src="assets/img/promo/1.jpg" alt="">
                         </div>
                         <div class="promo-body">
                             <h2 class="promo-name">product name goes here</h2>
                             <p class="promo-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id, labore.
                             </p>
                             <div class="promo-bar"></div>
                             <h2 class="promo-price">$900</h2>
                         </div>
                     </a>
                 </div>
             </div> -->

         </div>
         <div class="row" id="load_data_message"></div>

         <!-- <div class="data_msg" id=""></div> -->
     </div>
     <div class="loading" style="display: none;">
         <div class="content"><img src="<?php echo base_url() . 'assets/img/loading.gif'; ?>" /></div>
     </div>
 </div>
 <script>
     $(document).ready(function() {

         var limit = 8;
         var start = 0;
         var action = 'inactive';

         function loader(limit) {
             var output = '';
             for (var count = 0; count < limit; count++) {

                 output += '<div class="col-md-3">';
                 output += '<span class="content-placeholder">';
                 output += '</div>';
             }
             $('#load_data_message').html(output);
         }

         loader(limit);

         function listPromo(limit, start) {
             $.ajax({
                 method: 'POST',
                 url: '<?php echo site_url('home/list') ?>',
                 data: {
                     limit: limit,
                     start: start
                 },
                 cache: false,
                 success: function(data) {
                     if (data == '') {
                         $('#load_data_message').html('');
                         action = 'active';
                     } else {
                         $('#promotion').append(data);
                         $('#load_data_message').html("");
                         action = 'inactive';
                     }

                 }
             });
         }
         if (action == 'inactive') {
             action = 'active';
             listPromo(limit, start);
         }
         $(window).scroll(function() {
             if ($(window).scrollTop() + $(window).height() > $("#promotion").height() && action == 'inactive') {
                 loader(limit);
                 action = 'active';
                 start = start + limit;
                 setTimeout(function() {
                     listPromo(limit, start);
                 }, 1000);
             }
         });


     });
     var sendRequest = function() {
         var searchKey = $('#searchKey').val();
         if (searchKey === "") {
             swal("Error !", "Please input filed", "error");
             return false;
         } else {
             window.location.href = '<?= base_url('home/search_result') ?>?query=' + searchKey;
             $('.loading').show();
         }


     };
     var getNamedParameter = function(key) {
         if (key == undefined) return false;

         var url = window.location.href;
         console.log(url);
         var path_arr = url.split('?');
         if (path_arr.length === 1) {
             return null;
         }
         path_arr = path_arr[1].split('&');
         path_arr = remove_value(path_arr, "");
         var value = undefined;
         for (var i = 0; i < path_arr.length; i++) {
             var keyValue = path_arr[i].split('=');
             if (keyValue[0] == key) {
                 value = keyValue[1];
                 break;
             }
         }

         return value;
     };
     var remove_value = function(value, remove) {
         if (value.indexOf(remove) > -1) {
             value.splice(value.indexOf(remove), 1);
             remove_value(value, remove);
         }
         return value;
     };
     $('#searchKey').val(decodeURIComponent(getNamedParameter('query') || ""));
 </script>