  <div id="header">
      <div class="container">
          <div class="row">
              <div class="col-sm-3">
                  <div class="header-logo">
                      <a href="<?= site_url('home') ?>" class="logo"><img src="<?php echo base_url() ?>assets/img/logo.png" alt=""></a>
                  </div>
              </div>
              <div class="col-sm-7">
                  <div class="header-search">
                      <div class="search-div">
                          <input type="text" class="input" placeholder="Search here" id="searchKey" onchange="sendRequest()">
                          <a href="javascript:void(0);" class="search-btn" onclick="sendRequest();"> Search</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <nav class="navbar navbar-dark bg-light-color navbar-expand-sm">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-2" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="container">
          <div class="collapse navbar-collapse" id="navbar-list-2">
              <ul class="navbar-nav">
                  <li class="nav-item active">
                      <a class="nav-link" href="<?= site_url('home') ?>">Home</a>
                  </li>

                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                          Promo Category
                      </a>

                      <div class="dropdown-menu">
                          <?php
                            $category = $this->home_model->getCategories();
                            foreach ($category as $item) :
                            ?>
                              <a class="dropdown-item" href="<?= base_url('home/category_details/' . $item->id) ?>"><?= $item->category_name ?></a>
                          <?php endforeach; ?>
                      </div>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="<?= site_url('article/index/') ?>">Article</a>
                  </li>


              </ul>

          </div>
      </div>
  </nav>