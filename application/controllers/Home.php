<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->load->model('home_model');
    }

    private $response = array(
        'success' => '',
        'message' => '',
        'errorCode' => '',
        'data' => array()
    );
    private function sendResponse()
    {
        if ($this->response['errorCode']) {
            header('HTTP/1.1 ' . $this->response['errorCode'] . ' Internal Server');
            header('Content-Type: application/json; charset=UTF-8');
        } else {
            $this->response['success'] = true;
        }
        echo json_encode($this->response);
    }

    public function index()
    {
        $data['title'] = 'Promotion';
        $data['slider'] = $this->db->query('SELECT * FROM sliders ORDER BY created_at DESC')->result();
        $data['subview'] = $this->load->view('home/index', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function list()
    {
        $output = '';
        $data = $this->home_model->promo_list($this->input->post('limit'), $this->input->post('start'));
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $item) {
                $output .= '
                     <div class="col-md-3">
                            <div class="promotion">
                                <a href="' . site_url() . 'home/details/' . $item->id . '">
                                    <div class="promo-img">
                                        <img src="' . base_url(PROMOTION_IMAGE . $item->image) . '" alt="">
                                    </div>
                                    <div class="promo-body">
                                        <h2 class="promo-name">' . $item->title . '</h2>
                                        <p class="promo-text">' . $item->slug . '</p>
                                        <div class="promo-bar"></div>
                                        <h2 class="promo-price">' . $item->price . '</h2>
                                    </div>
                                </a>
                            </div>
                     </div>
                ';
            }
        }
        echo $output;
    }
    public function search_result()
    {
        $data['title'] = 'Promotion';
        $data['slider'] = $this->db->query('SELECT * FROM sliders ORDER BY created_at DESC')->result();
        $data['searchFor'] = ($this->input->get('query')) ? $this->input->get('query') : NULL;
        $data['list'] = $this->home_model->promo_list($this->input->post('limit'), $this->input->post('start'), $data['searchFor']);
        $data['subview'] = $this->load->view('home/search_result', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function details($id)
    {
        $data['title'] = 'Details';
        $data['list'] = $this->db->query('SELECT * FROM promotions WHERE id = ' . $id . '')->result();
        $data['subview'] = $this->load->view('home/details', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function category_details($id)
    {

        $data['title'] = 'Details';
        $data['slider'] = $this->db->query('SELECT * FROM sliders ORDER BY created_at DESC')->result();
        $data['category'] = $this->home_model->promoCategories($id);
        $data['subview'] = $this->load->view('home/category_details', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function articlePage()
    {
        $data['title'] = 'Article';
        $data['slider'] = $this->db->query('SELECT * FROM sliders ORDER BY created_at DESC')->result();
        $data['subview'] = $this->load->view('home/article', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function getArticle()
    {
        $output = '';
        $data = $this->home_model->article_list($this->input->post('limit'), $this->input->post('start'));
        if ($data->num_rows() > 0) {
            foreach ($data->result() as $item) {

                $output .= '
                     <div class="col-md-4">
                             <div class="art-div">
                                <a href="' . site_url() . 'home/article_details/' . $item->id . '">
                                    <img src="' . base_url(ARTICLE_IMAGE . $item->image) . '" class="img-art" alt="">
                                    <div class="art-content">
                                        <h3>' . $item->title . '</h3>
                                        <i class="fa fa-clock"><span>' . $this->time_ago($item->created_at) . '</span></i>
                                        <p>' . $item->slug_title . '</p>
                                    </div>
                                </a>
                            </div>
                     </div>
                ';
            }
        }
        echo $output;
    }

    public function article_details($id)
    {
        $data['result'] = $this->db->query('SELECT * FROM articles WHERE id ="' . $id . '"')->row_array();
        // echo '<pre>';
        // print_r($data['result']);
        $data['subview'] = $this->load->view('home/article_details', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    private function str_short($string, $length, $lastLength = 0, $symbol = '...')
    {
        if (strlen($string) > $length) {
            $result = substr($string, 0, $length - $lastLength - strlen($symbol)) . $symbol;
            return $result . ($lastLength ? substr($string, -$lastLength) : '');
        }

        return $string;
    }

    private function time_ago($timestamp)
    {

        date_default_timezone_set('Asia/Manila');
        $time_ago        = strtotime($timestamp);
        $current_time    = time();
        $time_difference = $current_time - $time_ago;
        $seconds         = $time_difference;

        $minutes = round($seconds / 60); // value 60 is seconds  
        $hours   = round($seconds / 3600); //value 3600 is 60 minutes * 60 sec  
        $days    = round($seconds / 86400); //86400 = 24 * 60 * 60;  
        $weeks   = round($seconds / 604800); // 7*24*60*60;  
        $months  = round($seconds / 2629440); //((365+365+365+365+366)/5/12)*24*60*60  
        $years   = round($seconds / 31553280); //(365+365+365+365+366)/5 * 24 * 60 * 60

        if ($seconds <= 60) {

            return "Just Now";
        } else if ($minutes <= 60) {

            if ($minutes == 1) {

                return "one minute ago";
            } else {

                return "$minutes minutes ago";
            }
        } else if ($hours <= 24) {

            if ($hours == 1) {

                return "an hour ago";
            } else {

                return "$hours hrs ago";
            }
        } else if ($days <= 7) {

            if ($days == 1) {

                return "yesterday";
            } else {

                return "$days days ago";
            }
        } else if ($weeks <= 4.3) {

            if ($weeks == 1) {

                return "a week ago";
            } else {

                return "$weeks weeks ago";
            }
        } else if ($months <= 12) {

            if ($months == 1) {

                return "a month ago";
            } else {

                return "$months months ago";
            }
        } else {

            if ($years == 1) {

                return "one year ago";
            } else {

                return "$years years ago";
            }
        }
    }
}
