<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

//dashboard
$route['user'] = 'admin/dashboard/list';

$route['home'] = 'home/index';

//========================
//admin
//========================
$route['users'] = 'admin/users/index';
$route['add_addmin'] = 'admin/users/addAdmins';
$route['admin_list'] = 'admin/users/listAdmin';
$route['admin_update'] = 'admin/users/update_admin';
$route['admin_delete'] = 'admin/users/delete_admin';
$route['change_password'] = 'admin/users/updatePassword';
$route['adminlogin'] = 'admin/auth';

//========================
//Promotion
//========================
$route['promotion-page'] = 'admin/promotion/index';
$route['add-promotion'] = 'admin/promotion/addPromotion';
$route['upload-promotion'] = 'admin/promotion/uploadPromotion';
$route['promotion-list'] = 'admin/promotion/listPromotion';
$route['promotion_delete'] = 'admin/promotion/delete_promotion';
$route['promotion_update'] = 'admin/promotion/update_content';
$route['update_image'] = 'admin/promotion/updateImage';
$route['seed_promotion'] = 'admin/promotion/seed_promotion';
$route['category'] = 'admin/promotion/categories';
$route['add-category'] = 'admin/promotion/addCategory';
$route['category-list'] = 'admin/promotion/listCategory';
$route['category-update'] = 'admin/promotion/update_category';
$route['category-delete'] = 'admin/promotion/delete_category';

//========================
//Article
//========================
$route['article'] = 'admin/article/index';
$route['add-article'] = 'admin/article/add_article';
$route['upload-article'] = 'admin/article/upload';
$route['list-article'] = 'admin/article/listArticle';
$route['delete-article'] = 'admin/article/deleteArticle';
//homepage
$route['article/index'] = 'home/articlePage';
$route['article/list'] = 'home/getArticle';

//========================
//login
//========================
$route['logout'] = 'admin/auth/logout';

//========================
//slider
//========================
$route['slider-page'] = 'admin/sliders/index';
$route['add-slider'] = 'admin/sliders/addSlider';
$route['upload-slider'] = 'admin/sliders/file_data';
$route['delete'] = 'admin/sliders/delete_slider';
$route['list'] = 'admin/sliders/slider_list';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
