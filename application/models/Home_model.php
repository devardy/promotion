<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function promo_list($limit, $start, $st = "")
    {
        $this->db->select('*');
        $this->db->from('promotions');
        $this->db->or_like('title', $st);
        $this->db->or_like('slug', $st);
        $this->db->or_like('price', $st);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query;
    }

    public function getCategories()
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function promoCategories($id)
    {
        $this->db->select('*');
        $this->db->from('promotions');
        $this->db->where('category_id', $id);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function article_list($limit, $start, $st = "")
    {
        $this->db->select('*');
        $this->db->from('articles');
        $this->db->or_like('title', $st);
        $this->db->or_like('content', $st);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query;
    }
}
