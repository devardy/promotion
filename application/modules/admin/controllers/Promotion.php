<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promotion extends MY_Controller
{
    private $response = array(
        'success' => '',
        'message' => '',
        'errorCode' => '',
        'data' => array()
    );


    public function __construct()
    {
        Parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('security');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->database();
        $this->load->model('promotion_model');
        $isLoggedIn = $this->session->userdata('islogin');
        if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            redirect('adminlogin');
        }
    }
    private function sendResponse()
    {
        if ($this->response['errorCode']) {
            header('HTTP/1.1 ' . $this->response['errorCode'] . ' Internal Server');
            header('Content-Type: application/json; charset=UTF-8');
        } else {
            $this->response['success'] = true;
        }
        echo json_encode($this->response);
    }

    public function index()
    {
        $data['title'] = 'Promotion';
        $data['category'] = $this->db->query('SELECT * FROM categories ORDER BY created_at DESC')->result();
        $data['subview'] = $this->load->view('promotion/list', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function addPromotion()
    {
        $data['title'] = 'Add Promotion';
        $data['category'] = $this->db->query('SELECT * FROM categories ORDER BY created_at DESC')->result();
        $data['subview'] = $this->load->view('promotion/add', $data, TRUE);
        $this->load->view('layouts', $data);
    }
    public function uploadPromotion()
    {
        //validate the form data 
        $this->form_validation->set_rules('title', 'Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('slug', 'Slug', 'required|trim|xss_clean');
        $this->form_validation->set_rules('category', 'Category', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'Content', 'required|trim');
        $this->form_validation->set_rules('price', 'Price', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Add Slider';
            $data['category'] = $this->db->query('SELECT * FROM categories ORDER BY created_at DESC')->result();
            $data['subview'] = $this->load->view('promotion/add', $data, TRUE);
            $this->load->view('layouts', $data);
        } else {
            //get the form values
            $data['title'] = $this->input->post('title', TRUE);
            $data['slug'] = $this->input->post('slug', TRUE);
            $data['category_id'] = $this->input->post('category', TRUE);
            $data['content'] = $this->input->post('content', FALSE);
            $data['price'] = $this->input->post('price', TRUE);
            $data['image'] = $this->input->post('image', TRUE);
            //file upload code 
            //set file upload settings 
            $config['upload_path']          = APPPATH . '../assets/uploads/promotion';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                //$error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
                $data['title'] = 'Add Slider';
                $data['error'] = $this->upload->display_errors();
                $data['category'] = $this->db->query('SELECT * FROM categories ORDER BY created_at DESC')->result();
                $data['subview'] = $this->load->view('promotion/add', $data, TRUE);
                $this->load->view('layouts', $data);
            } else {
                //file is uploaded successfully
                //now get the file uploaded data 
                $upload_data = $this->upload->data();
                //get the uploaded file name
                $data['image'] = $upload_data['file_name'];
                //store pic data to the db
                $query = $this->promotion_model->store_pic_data($data);
                if ($query = TRUE) {
                    $this->session->set_flashdata('success', 'Successfully added promotion !');
                    redirect('promotion-page/');
                }
            }
        }
    }
    public function save_promo_image($id = null)
    {
        if ($this->form_validation->run() == FALSE) {


            //file upload code 
            //set file upload settings 
            $config['upload_path']          = APPPATH . '../assets/uploads/promotion';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {

                if ($id = null) {
                    $data['title'] = 'Update Promotion Image';
                    $data['error'] = $this->upload->display_errors();
                    $data['image'] = $this->db->get_where('promotions', array('id' => $id))->row_array();
                    $data['subview'] = $this->load->view('promotion/edit', $data, TRUE);
                    $this->load->view('layouts', $data);
                }
            } else {
                //file is uploaded successfully
                //now get the file uploaded data 
                $upload_data = $this->upload->data();

                //get the uploaded file name
                $id = $this->input->post('id');
                $image = $upload_data['file_name'];
                $data = array(
                    'image' => $image,
                );
                //store pic data to the db
                $query = $this->promotion_model->save_update($id, $data);
                $this->session->set_flashdata('success', 'Successfully update promo image !');
                redirect('promotion-page/');
            }
        }
        $id = $this->input->post('id');
        $data['title'] = 'Update Promotion Image';
        $data['error'] = $this->upload->display_errors();
        $data['image'] = $this->db->get_where('promotions', array('id' => $id))->row_array();
        $data['subview'] = $this->load->view('promotion/edit', $data, TRUE);
        $this->load->view('layouts', $data);
    }
    public function listPromotion()
    {
        $column = array('title', 'category_id', 'content', 'price', 'image', 'created_at');

        $orderby = array($column[$this->input->get("order")[0]["column"]], $this->input->get("order")[0]["dir"]);
        // echo '<pre>';
        // print_r($orderby);
        $sorting = join(' ', $orderby);
        $offset = $this->input->get("length");
        $start = $this->input->get("start");
        $searchparam = $this->input->get("search")['value'];
        $data_list = $this->promotion_model->getPromotion($sorting, $offset, $start, $searchparam); //get data deposit query

        $data = array();
        $data["recordsFiltered"] = $this->promotion_model->count_promotion();
        $data['data'] = array();
        foreach ($data_list as $key => $value) {
            $data['data'][] = array(
                $value->image ? '<a href="javascript:void(0)"><img src="' . base_url(PROMOTION_IMAGE . $value->image) . '" id="img-custom" class="img-responsive" style="width:50px" /></a>' : '',
                $value->title,
                $value->category_name,
                $value,
                $value->price,
                date("D, d M g:i:A", strtotime($value->created_at)),
                $value
            );
        }

        $data["recordsTotal"] = count($data['data']);
        echo json_encode($data);
    }
    public function delete_promotion()
    {
        $id = $this->input->post('id');
        $this->promotion_model->delete_promotion($id);
    }

    public function update_content()
    {
        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $category = $this->input->post('category_id');
        $content = $this->input->post('content');
        $price = $this->input->post('price');
        if (empty($title) || empty($content) || empty($price)) {
            $this->response['message'] = ' Required Fields !';
            $this->response['errorCode'] = 406;
            $this->response['success'] = false;
        } else {
            $data = array(
                'title' => $title,
                'category_id' => $category,
                'content' => $content,
                'price' => $price
            );
            $this->promotion_model->update($id, $data);
        }
        $this->sendResponse();
    }

    public function updateImage($id)
    {
        if ($id) {
            $data['title'] = 'Update Promotion Image';
            $data['image'] = $this->db->get_where('promotions', array('id' => $id))->row_array();
            $data['subview'] = $this->load->view('promotion/edit', $data, TRUE);
            $this->load->view('layouts', $data);
        }
    }

    public function categories()
    {
        $data['title'] = 'Categories';
        $data['subview'] = $this->load->view('promotion/category', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function addCategory()
    {
        $category = $this->input->post('category_name');
        if (empty($category)) {
            $this->response['message'] = ' Required Fields !';
            $this->response['errorCode'] = 406;
            $this->response['success'] = false;
        } else {
            $data = array(
                'category_name' => $category,
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->promotion_model->add_category($data);
            $this->response['message'] = 'Successfully added category !';
            $this->response['success'] = true;
        }
        $this->sendResponse();
    }

    public function listCategory()
    {
        $column = array('category_name', 'created_at');

        $orderby = array($column[$this->input->get("order")[0]["column"]], $this->input->get("order")[0]["dir"]);
        // echo '<pre>';
        // print_r($orderby);
        $sorting = join(' ', $orderby);
        $offset = $this->input->get("length");
        $start = $this->input->get("start");
        $searchparam = $this->input->get("search")['value'];
        $data_list = $this->promotion_model->getCategory($sorting, $offset, $start, $searchparam); //get data deposit query

        $data = array();
        $data["recordsFiltered"] = $this->promotion_model->count_category();
        $data['data'] = array();
        foreach ($data_list as $key => $value) {
            $data['data'][] = array(
                $value->category_name,
                date("D, d M g:i:A", strtotime($value->created_at)),
                $value
            );
        }

        $data["recordsTotal"] = count($data['data']);
        echo json_encode($data);
    }

    public function update_category()
    {
        $id = $this->input->post('id');
        $category = $this->input->post('category_name');
        if (empty($category)) {
            $this->response['message'] = ' Required Fields !';
            $this->response['errorCode'] = 406;
            $this->response['success'] = false;
        } else {
            $data = array(
                'category_name' => $category,
            );
            $this->promotion_model->updateCategory($id, $data);
        }
        $this->sendResponse();
    }

    public function delete_category()
    {
        $id = $this->input->post('id');
        $this->promotion_model->deleteCategory($id);
    }

    public function seed_promotion()
    {
        $nameArr = array();
        for ($d = 100; count($nameArr) < $d;) {
            $randomString = $this->random_char(2, 5);
            if (!in_array($randomString, $nameArr, true)) {
                array_push($nameArr,  $randomString);
                $data = array(
                    'title' => $randomString,
                    'slug' => $randomString,
                    'price' => '200',
                    'image' => '' . base_url("assets/img/promo/3.jpg") . '"',
                );
                $this->promotion_model->seed_promotion($data);
            }
        }
        echo '<h1>100 user records added to database </h1></br>';
        print_r($nameArr);
    }
    private function random_char($minlen = 0, $maxlen = 6)
    {
        $characters = explode(" ", 'a b c d e f g h i j k l m n o p q r s t u ');
        $charactersLength = count($characters);
        $randloop = rand($minlen, $maxlen);
        $charc = array();
        for ($i = 0; $i < $randloop; $i++) {
            $charc[] = $characters[rand(0, $charactersLength - 1)];
        }
        return join('', $charc);
    }
}
