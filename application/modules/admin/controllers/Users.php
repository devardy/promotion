<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Controller
{
    private $response = array(
        'success' => '',
        'message' => '',
        'errorCode' => '',
        'data' => array()
    );
    public function __construct()
    {
        Parent::__construct();
        $this->load->model('users_model');
        $isLoggedIn = $this->session->userdata('islogin');
        if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            redirect('adminlogin');
        }
    }

    private function sendResponse()
    {
        if ($this->response['errorCode']) {
            header('HTTP/1.1 ' . $this->response['errorCode'] . ' Internal Server');
            header('Content-Type: application/json; charset=UTF-8');
        } else {
            $this->response['success'] = true;
        }
        echo json_encode($this->response);
    }

    public function index()
    {
        $data['title'] = 'Admin list';
        $data['subview'] = $this->load->view('users/list', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function addAdmins()
    {
        $fullname = $this->input->post('fullname');
        $password = hash('sha256', md5($this->input->post('password')));
        $username = $this->input->post('username');
        if (empty($fullname) || empty($password) || empty($username)) {
            $this->response['message'] = ' Required Fields !';
            $this->response['errorCode'] = 406;
            $this->response['success'] = false;
        } else {
            $data = array(
                'fullname' => $fullname,
                'password' => $password,
                'username' => $username,
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->users_model->add_admin($data);
            $this->response['message'] = 'Successfully added user !';
            $this->response['success'] = true;
        }
        $this->sendResponse();
    }
    public function listAdmin()
    {
        $column = array('fullname', 'username', 'created_at');
        $orderby = array($column[$this->input->get("order")[0]["column"]], $this->input->get("order")[0]["dir"]);
        $sorting = join(' ', $orderby);
        $offset = $this->input->get("length");
        $start = $this->input->get("start");
        $searchparam = $this->input->get("search")['value'];
        $data_list = $this->users_model->getAdminList($sorting, $offset, $start, $searchparam); //get data deposit query
        // echo '<pre>';
        // print_r($data_list);
        $data = array();
        $data["recordsFiltered"] = $this->users_model->count_admin();
        $data['data'] = array();
        foreach ($data_list as $key => $value) {
            $data['data'][] = array(
                $value->fullname,
                $value->username,
                date("D, d M g:i:A", strtotime($value->created_at)),
                $value
            );
        }
        $data["recordsTotal"] = count($data['data']);
        echo json_encode($data);
    }
    public function update_admin()
    {
        $id = $this->input->post('id');
        $fname = $this->input->post('fullname');
        $uname = $this->input->post('username');
        if (empty($fname) || empty($uname)) {
            $this->response['message'] = ' Required Fields !';
            $this->response['errorCode'] = 406;
            $this->response['success'] = false;
        } else {
            $data = array(
                'fullname' => $fname,
                'username' => $uname,
            );
            $this->users_model->update_admin($id, $data);
        }
        $this->sendResponse();
    }
    public function delete_admin()
    {
        $id = $this->input->post('id');
        $query = $this->db->query('DELETE admin FROM admin WHERE id ="' . $id . '"');
        if ($query == TRUE) {
            $this->response['message'] = 'Successfully delete account !';
            $this->response['success'] = true;
        }
    }

    //update password
    public function updatePassword()
    {
        $id = $this->input->post('id');
        $newPassword = hash('sha256', md5($this->input->post('password')));
        $this->db->query('UPDATE admin SET password = "' . $newPassword . '" WHERE id ="' . $id . '"');
    }
}
