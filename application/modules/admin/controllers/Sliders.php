<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sliders extends MY_Controller
{
    private $response = array(
        'success' => '',
        'message' => '',
        'errorCode' => '',
        'data' => array()
    );
    public function __construct()
    {
        Parent::__construct();
        $this->load->model('slider_model');
        $isLoggedIn = $this->session->userdata('islogin');
        if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            redirect('adminlogin');
        }
    }

    public function index()
    {
        $data['title'] = 'Slider list';
        $data['sliders'] = $this->db->query('SELECT * FROM sliders ORDER BY created_at DESC')->result();
        // echo '<pre>';
        // print_r($data['sliders']);
        $data['subview'] = $this->load->view('slider/list', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function slider_list()
    {
        $column = array('title', 'image', 'created_at');
        $orderby = array($column[$this->input->get("order")[0]["column"]], $this->input->get("order")[0]["dir"]);
        $sorting = join(' ', $orderby);
        $offset = $this->input->get("length");
        $start = $this->input->get("start");
        $searchparam = $this->input->get("search")['value'];
        $data_list = $this->slider_model->getSliders($sorting, $offset, $start, $searchparam); //get data deposit query

        $data = array();
        $data["recordsFiltered"] = $this->slider_model->count_slider();
        $data['data'] = array();
        foreach ($data_list as $key => $value) {
            $data['data'][] = array(
                $value->image ? '<a href="javascript:void(0)"><img src="' . base_url(SLIDER_IMAGE . $value->image) . '" id="img-custom" class="img-responsive" style="width:50px" /></a>' : '',
                $value->title,
                date("D, d M g:i:A", strtotime($value->created_at)),
                $value
            );
        }

        $data["recordsTotal"] = count($data['data']);
        echo json_encode($data);
    }

    public function addSlider()
    {
        $data['title'] = 'Add Slider';
        $data['subview'] = $this->load->view('slider/add_image', $data, TRUE);
        $this->load->view('layouts', $data);
    }
    public function file_data()
    {
        //validate the form data 
        $this->form_validation->set_rules('title', 'Image Title', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'Add Slider';
            $data['subview'] = $this->load->view('slider/add_image', $data, TRUE);
            $this->load->view('layouts', $data);
        } else {
            //get the form values
            $data['title'] = $this->input->post('title', TRUE);
            //file upload code 
            //set file upload settings 
            $config['upload_path']          = APPPATH . '../assets/uploads/slider';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                //$error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
                $data['title'] = 'Add Slider';
                $data['error'] = $this->upload->display_errors();
                $data['subview'] = $this->load->view('slider/add_image', $data, TRUE);
                $this->load->view('layouts', $data);
            } else {
                //file is uploaded successfully
                //now get the file uploaded data 
                $upload_data = $this->upload->data();
                //get the uploaded file name
                $data['image'] = $upload_data['file_name'];
                //store pic data to the db
                $query = $this->slider_model->store_pic_data($data);
                if ($query = TRUE) {
                    $this->session->set_flashdata('success', 'Successfully added slider !');
                    redirect('slider-page');
                }
            }
        }
    }
    public function delete_slider()
    {
        $id = $this->input->post('id');
        $this->slider_model->delete_slider($id);
    }
}
