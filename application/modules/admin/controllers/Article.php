<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends MY_Controller
{
    private $response = array(
        'success' => '',
        'message' => '',
        'errorCode' => '',
        'data' => array()
    );


    public function __construct()
    {
        Parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('security');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->helper('text');
        $this->load->database();
        $this->load->model('article_model');
        $isLoggedIn = $this->session->userdata('islogin');
        if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
            redirect('adminlogin');
        }
    }

    public function index()
    {
        $data['title'] = "Article";
        $data['subview'] = $this->load->view('article/index', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function listArticle()
    {
        $column = array('title', 'slug_title', 'image', 'created_at');

        $orderby = array($column[$this->input->get("order")[0]["column"]], $this->input->get("order")[0]["dir"]);
        // echo '<pre>';
        // print_r($orderby);
        $sorting = join(' ', $orderby);
        $offset = $this->input->get("length");
        $start = $this->input->get("start");
        $searchparam = $this->input->get("search")['value'];
        $data_list = $this->article_model->getArticle($sorting, $offset, $start, $searchparam); //get data deposit query

        $data = array();
        $data["recordsFiltered"] = $this->article_model->countArticle();
        $data['data'] = array();
        foreach ($data_list as $key => $value) {
            $data['data'][] = array(
                $value->image ? '<a href="javascript:void(0)"><img src="' . base_url(ARTICLE_IMAGE . $value->image) . '" id="img-custom" class="img-responsive" style="width:50px" /></a>' : '',
                $value->title,
                $value->slug_title,
                date("D, d M g:i:A", strtotime($value->created_at)),
                $value
            );
        }

        $data["recordsTotal"] = count($data['data']);
        echo json_encode($data);
    }

    public function add_article()
    {
        $data['title'] = "Add Article";
        $data['subview'] = $this->load->view('article/add', $data, TRUE);
        $this->load->view('layouts', $data);
    }

    public function upload()
    {
        //validate the form data 
        $this->form_validation->set_rules('title', 'Title', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content', 'Content', 'required|trim|xss_clean');
        $this->form_validation->set_rules('slug_title', 'Slug', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = 'upload';
            $data['subview'] = $this->load->view('article/add', $data, TRUE);
            $this->load->view('layouts', $data);
        } else {
            //get the form values
            $data['title'] = $this->input->post('title', TRUE);
            $data['slug_title'] = $this->input->post('slug_title', TRUE);
            $data['content'] = $this->input->post('content', FALSE);
            $data['image'] = $this->input->post('image', TRUE);
            //file upload code 
            //set file upload settings 
            $config['upload_path']          = APPPATH . '../assets/uploads/article';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')) {
                //$error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
                $data['title'] = 'Add article';
                $data['error'] = $this->upload->display_errors();
                $data['subview'] = $this->load->view('article/add', $data, TRUE);
                $this->load->view('layouts', $data);
            } else {
                //file is uploaded successfully
                //now get the file uploaded data 
                $upload_data = $this->upload->data();
                //get the uploaded file name
                $data['image'] = $upload_data['file_name'];
                //store pic data to the db
                $query = $this->article_model->store_pic_data($data);
                if ($query = TRUE) {
                    $this->session->set_flashdata('success', 'Successfully added article !');
                    redirect('article/');
                }
            }
        }
    }

    public function update_article($id = null)
    {
        if ($id) {
            $data['list'] = $this->db->get_where('articles', array('id' => $id))->row();
            $data['title'] = 'Edit';
            $data['subview'] = $this->load->view('article/edit', $data, TRUE);
            $this->load->view('layouts', $data);
        }
    }

    public function saveArticle()
    {
        $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
        $this->form_validation->set_rules('slug_title', 'Title slug', 'trim|required|xss_clean');
        $this->form_validation->set_rules('content', 'Content', 'trim|required|xss_clean');
        if ($this->form_validation->run() == TRUE) {

            $id = $this->input->post('id');
            $data['title'] = $this->input->post('title');
            $data['slug_title'] = $this->input->post('slug_title');
            $data['content'] = $this->input->post('content');

            if (!empty($id)) {

                $this->db->where('id', $id);
                $this->db->update('articles', $data);
                handel_upload_article($id);
            } else {
                $this->db->insert('articles', $data);
                $last_insert = $this->db->insert_id();
                handel_upload_article($last_insert);
            }
            $this->message->save_success('admin/article/index');
        } else {
            $error = validation_errors();
            $this->message->custom_error_msg('admin/article/update_article', $error);
        }
    }

    public function remove($id)
    {
        $file = $this->db->get_where('articles', array("id" => $id))->row();
        $file = ARTICLE_IMAGE . $file->image;
        if (!unlink($file)) {
            //error
            $this->message->custom_error_msg('admin/article/update_article/' . $id, "Error deleting $file");
        } else {
            //success
            $this->db->where('id', $id);
            $this->db->update('articles', array('image' => ''));
            $this->message->delete_success('admin/article/update_article/' . $id);
        }
    }

    public function deleteArticle()
    {
        $id = $this->input->post('id');
        $this->article_model->delete_article($id);
    }
}
