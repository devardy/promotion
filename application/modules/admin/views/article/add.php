             <div class="col-md-12">
                 <div class="card card-secondary">
                     <div class="card-header">
                         <h3 class="card-title"><?php echo $title ?></h3>
                     </div>
                     <!-- /.card-header -->

                     <!-- form start -->
                     <div class="col-md-4" style="color:red">
                         <?php echo validation_errors(); ?>
                         <?php if (isset($error)) {
                                print $error;
                            } ?>
                     </div>
                     <form action="<?php echo site_url('upload-article') ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                         <div class="card-body">
                             <div class="form-group">
                                 <label>Title</label>
                                 <input type="text" class="form-control" name="title" value="<?= set_value('title'); ?>" placeholder="title">
                             </div>
                             <div class="form-group">
                                 <label>Title Slug</label>
                                 <input type="text" class="form-control" name="slug_title" value="<?= set_value('slug_title'); ?>" placeholder="title slug">
                             </div>
                             <div class="form-group">
                                 <label>Content</label>
                                 <textarea class="form-control" id="editor1" name="content" style="height: 300px"></textarea>
                             </div>
                             <div class="form-group">
                                 <label for="customFile">Article Image</label>
                                 <div class="custom-file">
                                     <input type="file" class="custom-file-input" id="customFile" name="image">
                                     <label class="custom-file-label" for="customFile">Choose file</label>
                                 </div>
                             </div>

                         </div>
                         <!-- /.card-body -->

                         <div class="card-footer">
                             <button type="submit" class="btn btn-primary">Submit</button>
                             <a href="<?php echo site_url("article") ?>" class="btn btn-danger">Back</a>
                         </div>
                     </form>
                 </div>
             </div>
             <script>
                 $(function() {
                     // Replace the <textarea id="editor1"> with a CKEditor
                     // instance, using default configuration.
                     CKEDITOR.replace('editor1');
                 });
             </script>