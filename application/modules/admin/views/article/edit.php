             <div class="col-md-12">
                 <div class="card card-secondary">
                     <div class="card-header">
                         <h3 class="card-title"><?php echo $title ?></h3>
                     </div>
                     <!-- /.card-header -->

                     <!-- form start -->
                     <div class="col-md-4" style="color:red">
                         <?php echo validation_errors(); ?>
                         <?php if (isset($error)) {
                                print $error;
                            } ?>
                     </div>

                     <div class="row justify-content-md-center">
                         <div class="col-md-8">
                             <?php echo message_box('success'); ?>
                             <?php echo message_box('error'); ?>
                             <form action="<?php echo site_url('admin/article/saveArticle') ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                 <div class="card-body">
                                     <?php if (!empty($list)) {
                                            if ($list->image != NULL && $list->image != '') { ?>
                                             <div class="row" style="padding-top: 25px; padding-bottom: 30px">
                                                 <div class="col-md-4">
                                                     <label>Image Article </label>
                                                     <img height="150" width="220" src="<?php echo site_url(ARTICLE_IMAGE . $list->image) ?>" class="img img-responsive">
                                                 </div>
                                                 <div class="col-md-8 text-left" style="padding-top: 20px">
                                                     <a data-original-title="Delete" data-toggle="tooltip" title="" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete ?')" href="<?php echo site_url('admin/article/remove/' . $list->id) ?>"><i class="fa fa-trash"></i></a>
                                                 </div>
                                             </div>
                                             <hr />

                                         <?php } else { ?>
                                             <div class="form-group">
                                                 <label>Image Article </label>
                                                 <div class="box">
                                                     <input type="file" name="image" id="file-1" class="imgfile imgfile-1" data-multiple-caption="{count} files selected" />
                                                     <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                                             <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" />
                                                         </svg> <span>Choose a file&hellip;</span></label>
                                                 </div>
                                             </div>
                                         <?php }
                                        } else { ?>
                                         <div class="form-group">
                                             <label>Image Promo </label>
                                             <div class="box">
                                                 <input type="file" name="image" id="file-1" class="imgfile imgfile-1" data-multiple-caption="{count} files selected" />
                                                 <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                                                         <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z" />
                                                     </svg> <span>Choose a file&hellip;</span></label>
                                             </div>
                                         </div>
                                     <?php } ?>

                                     <div class="form-group">
                                         <label>Title</label>
                                         <input type="text" class="form-control" name="title" value="<?php if (!empty($list)) echo $list->title ?>" placeholder="title">
                                     </div>
                                     <div class="form-group">
                                         <label>Title Slug</label>
                                         <input type="text" class="form-control" name="slug_title" value="<?php if (!empty($list)) echo $list->slug_title ?>" placeholder="title slug">
                                     </div>
                                     <div class="form-group">
                                         <label>Content</label>
                                         <textarea class="form-control" id="editor1" name="content" style="height: 300px"><?php if (!empty($list)) echo $list->content ?></textarea>
                                     </div>

                                 </div>
                                 <!-- /.card-body -->

                                 <div class="card-footer">
                                     <input type="hidden" value="<?php if (!empty($list)) echo $list->id ?>" name="id">
                                     <button type="submit" class="btn btn-primary">Submit</button>
                                     <a href="<?php echo site_url("article") ?>" class="btn btn-danger">Back</a>
                                 </div>
                             </form>
                         </div>
                     </div>

                 </div>
             </div>
             <script>
                 $(function() {
                     // Replace the <textarea id="editor1"> with a CKEditor
                     // instance, using default configuration.
                     CKEDITOR.replace('editor1');
                 });
             </script>