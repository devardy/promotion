             <div class="col-md-12">
                 <div class="card card-secondary">
                     <div class="card-header">
                         <h3 class="card-title"><?php echo $title ?></h3>
                     </div>
                     <!-- /.card-header -->

                     <!-- form start -->
                     <div class="col-md-4" style="color:red">
                         <?php echo validation_errors(); ?>
                         <?php if (isset($error)) {
                                print $error;
                            } ?>
                     </div>
                     <div class="col-sm-3">
                         <div class="form-group">
                             <div class="img-thumbnail"><img width="200" src="<?php if (!empty($image)) echo base_url() . 'assets/uploads/promotion/' . $image['image'] ?>" alt=""></div>
                         </div>
                     </div>
                     <form action="<?php echo site_url('admin/promotion/save_promo_image') ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                         <div class="card-body">
                             <div class="form-group">
                                 <label for="customFile">Promo Image</label>
                                 <div class="custom-file">
                                     <input type="file" class="custom-file-input" id="customFile" name="image">
                                     <label class="custom-file-label" for="customFile">Choose file</label>
                                 </div>
                             </div>
                         </div>
                         <!-- /.card-body -->

                         <div class="card-footer">
                             <input type="hidden" value="<?php if (!empty($image['id'])) echo $image['id'] ?>" name="id">
                             <button type="submit" class="btn btn-primary">Submit</button>
                             <a href="<?php echo site_url("promotion-page") ?>" class="btn btn-danger">Back</a>
                         </div>
                     </form>
                 </div>
             </div>