<div class="row">
  <div class="col-md-12">
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"><?php echo $title; ?></h3>
      </div>
      <div class="card-body">
        <div class="col-md-2">
          <a href="<?php echo site_url("add-promotion"); ?>" class="btn btn-warning"> <i class="fa fa-image"></i> Add Promotion</a>
        </div><br>
        <?php if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success !</strong> <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } else if ($this->session->flashdata('error')) {  ?>
          <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error !</strong> <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <div class="col-md-12">
          <table id="promotion" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Image</th>
                <th>Title </th>
                <th>Category </th>
                <th>Content</th>
                <th>Price</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>

          </table>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
<!--  Modal image -->
<div class="modal fade" id="imagemodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <img src="" id="imagepreview" class="img-thumbnail">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal update-->
<div id="Edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Content</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="">Title</label>
          <input type="text" class="form-control" name="title" id="title">
        </div>
        <div class="form-group">
          <label for="">Category</label>
          <select class="form-control" name="category" id="category">
            <?php foreach ($category as $item) : ?>
              <option value="<?= $item->id ?>"><?= $item->category_name ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group">
          <label for="">Content</label>
          <textarea class="form-control" rows="3" name="content" id="content"></textarea>
        </div>
        <div class="form-group">
          <label for="">Price</label>
          <input type="text" class="form-control" name="price" id="price">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnUpdate">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
  var manageTable = null;
  $(document).ready(function() {
    $('body').on("click", "#img-custom", function() {
      var img = $(this).attr('src');
      $('#imagepreview').attr('src', img);
      $('#imagemodal').modal('show');
    });
    manageTable = $('#promotion').DataTable({
      "processing": true,
      "serverSide": true,
      "searching": true,
      "lengthMenu": [
        [10, 25, 50, 100],
        [10, 25, 100, "All"]
      ],
      "order": [
        [5, "desc"]
      ],
      "columnDefs": [{
        "targets": 3,
        "orderable": false,
        "render": function(data, type, row) {
          var html = ' <textarea class="form-control" rows="3">' + data.content + '</textarea>';
          return html;
        }
      }, {
        "targets": 6,
        "orderable": false,
        "render": function(data, type, row) {
          var html = ' <div class="btn-group">' +
            '<a onclick="list_methods.update(this);" href="javascript:void(0)" data-id=' + data.id + ' data-title=' + JSON.stringify(data.title) + ' data-category=' + data.category_id + ' data-content=' + JSON.stringify(data.content) + ' data-price=' + JSON.stringify(data.price) + ' data-toggle="tooltip" data-placement="top" title="update" class=" btn btn-primary btn-flat"><i class="fa fa-edit"></i></a>' +
            '<a href="<?php echo site_url() ?>admin/promotion/updateImage/' + data.id + '" data-toggle="tooltip" data-placement="top" title="update image" class=" btn btn-success btn-flat"><i class="fa fa-image"></i></a>' +
            '<a onclick="list_methods.delete(this);" data-id=' + data.id + ' data-toggle="tooltip" data-placement="top" title="delete" class=" btn btn-danger btn-flat" href="javascript:void(0)"><i class="fa fa-trash"></i></a>' +

            '</div>';
          return html;
        }
      }],

      "language": {
        "processing": "Processing...",
        "loadingRecords": "Loading...",
        "lengthMenu": "Show _MENU_ entries",
        "zeroRecords": "No matching records found",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "Showing 0 to 0 of 0 entries",
        "infoFiltered": "(filtered from _MAX_ total entries)",
        "infoPostFix": "",
        "search": "Search:",
        "paginate": {
          "first": "First",
          "last": "Last",
          "next": "Next",
          "previous": "Previous"
        },
        "aria": {
          "sortAscending": ": Ascending",
          "sortDescending": ": Descending"
        }
      },
      "ajax": {
        "url": "<?php echo base_url(); ?>" + 'promotion-list',

      },

    });
    // console.log(manageTable);
  });
  var list_methods = {
    delete: function(e) {
      Swal.fire({
        title: 'Are you sure?',
        text: "You want to delete promotion ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then(function(result) {
        if (result.value) {
          var $data = $(e).data();
          var data = {
            id: $data.id,
          }
          apis.deletePromotion(data).then(function(res) {
            Swal.fire({
              icon: "success",
              text: "Successfully delete promotion ",
            });
            manageTable.ajax.reload();

          });

        }
      })
    },
    update: function(e) {
      var $data = $(e).data();
      var $modal = $('#Edit').modal(); //call modal

      $modal.find($('#title').val($data.title));
      $modal.find($('#content').val($data.content));
      $modal.find($('#price').val($data.price));
      $modal.find($('#category option[value=' + $data.category + ']').attr('selected', 'selected'));

      $modal.find('#btnUpdate').on('click', function() {
        $title = $.trim($modal.find('#title').val());
        $content = $.trim($modal.find('#content').val());
        $price = $.trim($modal.find('#price').val());
        $category = $.trim($modal.find('#category').val());
        if ($title == "" || $content == "" || $price == "" || $category == "") {
          Swal.fire({
            icon: "error",
            text: "Required! fields !",
          });
          return false;
        }
        $.ajax({
          type: 'POST',
          cache: false,
          url: '<?php echo base_url() ?>promotion_update',
          data: {
            //insert new update
            id: $data.id,
            title: $.trim($('#title').val()),
            category_id: $.trim($('#category').val()),
            content: $.trim($('#content').val()),
            price: $.trim($('#price').val()),
          }
        }).then(function(data, res, xhr) {
          if (xhr.status == 200) { //ok
            $modal.modal('hide');
            Swal.fire({
              icon: "success",
              text: "Your data has been updated !",
            });
            setTimeout(function() {
              location.reload(); // then reload the page
            }, 1300);
          }
        }, function(data) {
          toastr.error(data.responseJSON.message);
        })
      });
    }
  }
</script>