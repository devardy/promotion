<div class="row">
    <div class="col-md-12">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?php echo $title; ?></h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Add Category</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Category name</label>
                                        <input type="text" class="form-control" id="category" placeholder="category">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary" onclick="methods.addCategory();">Add category</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <table id="categoryList" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Date Created </th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                        </table>
                    </div>

                </div>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
<!--  Modal image -->
<div class="modal fade" id="imagemodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="" id="imagepreview" class="img-thumbnail">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal update-->
<div id="Edit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Category</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Category name</label>
                    <input type="text" class="form-control" name="category" id="category_id">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnUpdate">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    var manageTable = null;
    var list_methods = {
        delete: function(e) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete promotion ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function(result) {
                if (result.value) {
                    var $data = $(e).data();
                    var data = {
                        id: $data.id,
                    }
                    apis.deleteCategory(data).then(function(res) {
                        Swal.fire({
                            icon: "success",
                            text: "Successfully delete category ",
                        });
                        manageTable.ajax.reload();

                    });

                }
            })
        },
        update: function(e) {
            var $data = $(e).data();
            var $modal = $('#Edit').modal(); //call modal

            $modal.find($('#category_id').val($data.category));

            $modal.find('#btnUpdate').on('click', function() {
                $category = $.trim($modal.find('#category_id').val());
                if ($category == "") {
                    Swal.fire({
                        icon: "error",
                        text: "Required! fields !",
                    });
                    return false;
                }
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '<?php echo base_url() ?>category-update',
                    data: {
                        //insert new update
                        id: $data.id,
                        category_name: $.trim($('#category_id').val()),
                    }
                }).then(function(data, res, xhr) {
                    if (xhr.status == 200) { //ok
                        $modal.modal('hide');
                        Swal.fire({
                            icon: "success",
                            text: "Your data has been updated !",
                        });
                        manageTable.ajax.reload();
                    }
                }, function(data) {
                    toastr.error(data.responseJSON.message);
                })
            });
        }
    }

    $(document).ready(function() {
        $('body').on("click", "#img-custom", function() {
            var img = $(this).attr('src');
            $('#imagepreview').attr('src', img);
            $('#imagemodal').modal('show');
        });
        manageTable = $('#categoryList').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": true,
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 100, "All"]
            ],
            "order": [
                [1, "desc"]
            ],
            "columnDefs": [{
                "targets": 2,
                "orderable": false,
                "render": function(data, type, row) {
                    var html = ' <div class="btn-group">' +
                        '<a onclick="list_methods.update(this);" href="javascript:void(0)" data-id=' + data.id + ' data-category=' + data.category_name + ' data-toggle="tooltip" data-placement="top" title="update" class=" btn btn-primary btn-flat"><i class="fa fa-edit"></i></a>' +
                        '<a onclick="list_methods.delete(this);" data-id=' + data.id + ' data-toggle="tooltip" data-placement="top" title="delete" class=" btn btn-danger btn-flat" href="javascript:void(0)"><i class="fa fa-trash"></i></a>' +
                        '</div>';
                    return html;
                }
            }],

            "language": {
                "processing": "Processing...",
                "loadingRecords": "Loading...",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "Showing 0 to 0 of 0 entries",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "search": "Search:",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Previous"
                },
                "aria": {
                    "sortAscending": ": Ascending",
                    "sortDescending": ": Descending"
                }
            },
            "ajax": {
                "url": "<?php echo base_url(); ?>" + 'category-list',

            },

        });
        // console.log(manageTable);
    });
</script>