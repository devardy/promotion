<div class="row">
    <div class="col-md-12">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?php echo $title; ?></h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Add admin</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Fullname</label>
                                        <input type="text" class="form-control" id="fullname" placeholder="fullname">
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" id="username" placeholder="username">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="password">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary" onclick="methods.addAdmin();">Add admin</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <table id="adminList" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Fullname</th>
                                    <th>Username</th>
                                    <th>Date Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
<!-- Modal update-->
<div id="Edit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Admin User</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Fullname</label>
                    <input type="text" class="form-control" name="fullname" id="fname">
                </div>
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" class="form-control" name="username" id="uname">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnUpdate">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- Modal change password-->
<div id="change" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Admin User</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">New Password</label>
                    <input type="text" class="form-control" name="password" id="newPassowrd" placeholder="new password">
                </div>
                <div class="form-group">
                    <label for="">Comfirm Password</label>
                    <input type="text" class="form-control" name="cpassword" id="cPassword" placeholder="confirm password">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_change">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    var listTable = null;
    $(document).ready(function() {
        listTable = $('#adminList').DataTable({
            " processing": true,
            "serverSide": true,
            "searching": true,
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 100, "All"]
            ],
            "order": [
                [2, "desc"]
            ],
            "columnDefs": [{
                "targets": 3,
                "orderable": false,
                "render": function(data, type, row) {
                    var html = '<div class="btn-group">' +
                        '<a onclick="list_methods.edit(this);" data-id=' + data.id + ' data-fullname=' + JSON.stringify(data.fullname) + ' data-username=' + data.username + ' href="javascript:void(0)" class="btn btn-primary btn-flat" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i></a>' +
                        '<a onclick="list_methods.change_pass(this);" data-id=' + data.id + ' href="javascript:void(0)"  class="btn btn-warning btn-flat" data-toggle="tooltip" data-placement="top" title="change password"><i class="fas fa-lock"></i></a>' +
                        '<a onclick="list_methods.delete(this);" data-id=' + data.id + ' href="javascript:void(0)"  class="btn btn-danger btn-flat" data-toggle="tooltip" data-placement="top" title="delete"><i class="fas fa-trash"></i></a>' +
                        '</div>';
                    return html;
                },
            }, ],
            "language": {
                "processing": "Processing...",
                "loadingRecords": "Loading...",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "Showing 0 to 0 of 0 entries",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "search": "Search:",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Previous"
                },
                "aria": {
                    "sortAscending": ": Ascending",
                    "sortDescending": ": Descending"
                }
            },
            "ajax": {
                "url": "<?php echo base_url(); ?>" + 'admin_list',
            },
        });
    });
    var list_methods = {
        edit: function(e) {
            var $data = $(e).data();
            var $modal = $('#Edit').modal(); //call modal

            $modal.find($('#fname').val($data.fullname));
            $modal.find($('#uname').val($data.username));

            $modal.find('#btnUpdate').on('click', function() {
                $fname = $.trim($modal.find('#fname').val());
                $uname = $.trim($modal.find('#uname').val());
                if ($fname == "" || $uname == "") {
                    Swal.fire({
                        icon: "Error",
                        text: "Required! fields",
                    });

                    return false;
                }
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '<?php echo base_url() ?>admin_update',
                    data: {
                        //insert new update
                        id: $data.id,
                        fullname: $.trim($('#fname').val()),
                        username: $.trim($('#uname').val()),
                    }
                }).then(function(data, res, xhr) {
                    if (xhr.status == 200) { //ok
                        $modal.modal('hide');
                        Swal.fire({
                            icon: "success",
                            text: "Your data has been updated ",
                        });
                        listTable.ajax.reload();
                    }
                }, function(data) {
                    toastr.error(data.responseJSON.message);
                })
            });
        },
        delete: function(e) {

            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete users ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function(result) {
                if (result.value) {
                    var $data = $(e).data();
                    var data = {
                        id: $data.id,
                    }
                    apis.delete_admin(data).then(function(res) {
                        Swal.fire({
                            icon: "success",
                            text: "Successfully delete users ",
                        });
                        listTable.ajax.reload();
                    });

                }
            })
        },
        change_pass: function(e) {
            var $data = $(e).data();
            $('.modal-title').text('Change Password'); // set title
            var $modal = $('#change').modal(); //call modal

            $modal.find('#btn_change').on('click', function() {
                $pass = $.trim($modal.find('#newPassowrd').val());
                $cpass = $.trim($modal.find('#cPassword').val());

                if ($pass == "" || $cpass == "") {
                    toastr.error('Required fields');
                    listTable.ajax.reload();
                    return false;
                }
                if ($pass !== $cpass || !($pass + $cpass)) {
                    toastr.error('Password confirmation does not match with new password!');
                    listTable.ajax.reload();
                    return false;
                }
                $modal.find('#btn_change').html('saving...').addClass('disabled');
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url(); ?>change_password',
                    data: {
                        id: $.trim($data.id),
                        password: $pass
                    }
                }).always(function(data, res, xhr) {
                    $modal.find('#btn_change').html('save').removeClass('disabled');
                    if (xhr.status == 200) {
                        $modal.modal('hide');
                        $modal.find('#newPassowrd').val("");
                        $modal.find('#cPassword').val("");
                        Swal.fire({
                            icon: "success",
                            text: "Successfully changed the password",
                        });
                        listTable.ajax.reload();
                    }

                });
            });
        }
    }
</script>