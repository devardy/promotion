<div class="row">
    <div class="col-md-12">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><?php echo $title; ?></h3>
            </div>
            <div class="card-body">
                <div class="col-md-2">
                    <a href="<?php echo site_url("add-slider"); ?>" class="btn btn-warning"> <i class="fa fa-image"></i> Add Slider</a>
                </div><br>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Success !</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } else if ($this->session->flashdata('error')) {  ?>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error !</strong> <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <div class="col-md-12">
                    <table id="slider" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Title </th>
                                <th>Action</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
<!--  Modal image -->
<div class="modal fade" id="imagemodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="" id="imagepreview" class="img-thumbnail">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
    var list_methods = {
        delete: function(e) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to delete slider ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function(result) {
                if (result.value) {
                    var $data = $(e).data();
                    var data = {
                        id: $data.id,
                    }
                    apis.deleteSlider(data).then(function(res) {
                        Swal.fire({
                            icon: "success",
                            text: "Successfully delete slider ",
                        });
                        //list_provider.ajax.reload();
                        setTimeout(function() {
                            location.reload(); // then reload the page
                        }, 1300);
                    });

                }
            })
        }
    }
    var manageTable = null;
    $(document).ready(function() {
        $('body').on("click", "#img-custom", function() {
            var img = $(this).attr('src');
            $('#imagepreview').attr('src', img);
            $('#imagemodal').modal('show');
        });
        manageTable = $('#slider').DataTable({
            "processing": true,
            "serverSide": true,
            "searching": true,
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 100, "All"]
            ],
            "order": [
                [2, "desc"]
            ],
            "columnDefs": [{
                "targets": 2,
                "orderable": false,
                "render": function(data, type, row) {
                    var html = ' <div class="btn-group">' +
                        '<a onclick="list_methods.delete(this);" data-id=' + data.id + ' data-toggle="tooltip" data-placement="top" title="delete" class=" btn btn-danger btn-flat" href="javascript:void(0)"><i class="fa fa-trash"></i></a>' +
                        '</div>';
                    return html;
                }
            }],

            "language": {
                "processing": "Processing...",
                "loadingRecords": "Loading...",
                "lengthMenu": "Show _MENU_ entries",
                "zeroRecords": "No matching records found",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "Showing 0 to 0 of 0 entries",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "infoPostFix": "",
                "search": "Search:",
                "paginate": {
                    "first": "First",
                    "last": "Last",
                    "next": "Next",
                    "previous": "Previous"
                },
                "aria": {
                    "sortAscending": ": Ascending",
                    "sortDescending": ": Descending"
                }
            },
            "ajax": {
                "url": "<?php echo base_url(); ?>" + 'list',

            },

        });
        // console.log(manageTable);
    });
</script>