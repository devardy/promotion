             <div class="col-md-12">
                 <div class="card card-secondary">
                     <div class="card-header">
                         <h3 class="card-title"><?php echo $title ?></h3>
                     </div>
                     <!-- /.card-header -->

                     <!-- form start -->
                     <div class="col-md-4" style="color:red">
                         <?php echo validation_errors(); ?>
                         <?php if (isset($error)) {
                                print $error;
                            } ?>
                     </div>
                     <form action="<?php echo site_url('upload-slider') ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                         <div class="card-body">
                             <div class="form-group">
                                 <label>Title</label>
                                 <input type="text" class="form-control" name="title" value="<?= set_value('title'); ?>" placeholder="title">
                             </div>
                             <div class="form-group">
                                 <label for="exampleInputFile">Image</label>
                                 <div class="input-group">
                                     <div class="custom-file">
                                         <input type="file" class="custom-file-input" name="image" id="exampleInputFile">
                                         <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                     </div>
                                     <div class="input-group-append">
                                         <span class="input-group-text">Upload</span>
                                     </div>
                                 </div>
                             </div>

                         </div>
                         <!-- /.card-body -->

                         <div class="card-footer">
                             <button type="submit" class="btn btn-primary">Submit</button>
                             <a href="<?php echo site_url("slider-page") ?>" class="btn btn-danger">Back</a>
                         </div>
                     </form>
                 </div>
             </div>