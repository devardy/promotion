 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="../../index3.html" class="brand-link">
     <img src="<?php echo base_url(); ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
     <span class="brand-text font-weight-light">Admin Promotion</span>
   </a>

   <!-- Sidebar -->
   <div class="sidebar">
     <!-- Sidebar user (optional) -->
     <!-- Sidebar Menu -->
     <nav class="mt-2">
       <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item has-treeview <?php echo ($this->router->fetch_class() == 'promotion' ? 'menu-open' : ''); ?>">
           <a href="#" class="nav-link <?php echo ($this->router->fetch_class() == 'promotion' ? 'active' : ''); ?>">
             <i class="nav-icon fas fa-tachometer-alt"></i>
             <p>
               Promotion
               <i class="right fas fa-angle-left"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a href="<?php echo site_url("promotion-page") ?>" class="nav-link <?php if (in_array($this->uri->uri_string(), array('promotion-page', 'promotion-page'))) echo 'active'; ?>">
                 <i class="far fa-circle nav-icon"></i>
                 <p>List</p>
               </a>
             </li>
             <li class="nav-item">
               <a href="<?php echo site_url("category") ?>" class="nav-link <?php if (in_array($this->uri->uri_string(), array('category', 'category'))) echo 'active'; ?>">
                 <i class="far fa-circle nav-icon"></i>
                 <p>Categories</p>
               </a>
             </li>

           </ul>
         </li>

         <li class="nav-item has-treeview <?php echo ($this->router->fetch_class() == 'article' ? 'menu-open' : ''); ?>">
           <a href="#" class="nav-link  <?php echo ($this->router->fetch_class() == 'article' ? 'active' : ''); ?>">
             <i class="nav-icon fas fa-address-book"></i>
             <p>
               Article
               <i class="right fas fa-angle-left"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a href="<?php echo site_url('article') ?>" class="<?php if (in_array($this->uri->uri_string(), array('article', 'article'))) echo 'active'; ?> nav-link">
                 <i class="far fa-circle nav-icon"></i>
                 <p>List</p>
               </a>
             </li>

           </ul>
         </li>


         <li class="nav-item has-treeview <?php echo ($this->router->fetch_class() == 'users' ? 'menu-open' : ''); ?>">
           <a href="#" class="nav-link  <?php echo ($this->router->fetch_class() == 'users' ? 'active' : ''); ?>">
             <i class="nav-icon fas fa-users"></i>
             <p>
               Users
               <i class="right fas fa-angle-left"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a href="<?php echo site_url('users') ?>" class="<?php if (in_array($this->uri->uri_string(), array('users', 'users'))) echo 'active'; ?> nav-link">
                 <i class="far fa-circle nav-icon"></i>
                 <p>Admin List</p>
               </a>
             </li>

           </ul>
         </li>

         <li class="nav-item has-treeview <?php echo ($this->router->fetch_class() == 'sliders' ? 'menu-open' : ''); ?>">
           <a href="#" class="nav-link  <?php echo ($this->router->fetch_class() == 'sliders' ? 'active' : ''); ?>">
             <i class="nav-icon fas fa-image"></i>
             <p>
               Slider
               <i class="right fas fa-angle-left"></i>
             </p>
           </a>
           <ul class="nav nav-treeview">
             <li class="nav-item">
               <a href="<?php echo site_url('slider-page') ?>" class="<?php if (in_array($this->uri->uri_string(), array('slider-page', 'slider-page'))) echo 'active'; ?> nav-link">
                 <i class="far fa-circle nav-icon"></i>
                 <p>Slider List</p>
               </a>
             </li>

           </ul>
         </li>


       </ul>
     </nav>
     <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
 </aside>