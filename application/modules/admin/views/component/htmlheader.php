<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Promotion</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin.css?<?php echo time(); ?>">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sweetalert2.min.css?<?php echo time(); ?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/summernote/summernote-bs4.css?<?php echo time(); ?>">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/toastr/toastr.min.css?<?php echo time(); ?>">
  </link>
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url(); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- bs-custom-file-input -->
  <script src="<?php echo base_url(); ?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <!-- Summernote -->
  <script src="<?php echo base_url(); ?>plugins/summernote/summernote-bs4.min.js"></script>
  <!-- Editor wys -->
  <script src="https://cdn.ckeditor.com/4.9.0/full/ckeditor.js"></script>
  <script>
    var base_url = '<?php echo base_url(); ?>';
  </script>
  <script>
    $(function() {
      bsCustomFileInput.init();
      $('[data-toggle="tooltip"]').tooltip();
      $(window).bind("load", function() {
        window.setTimeout(function() {
          $(".alert")
            .fadeTo(500, 0)
            .slideUp(500, function() {
              $(this).remove();
            });
        }, 4000);
      });
      $('#summernote').summernote()
    });
  </script>
  <style>
    .modal-header {
      display: initial !important;
    }
  </style>