<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Article_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //save picture data to db
    public function store_pic_data($data)
    {
        $insert_data['title'] = $data['title'];
        $insert_data['slug_title'] = $data['slug_title'];
        $insert_data['content'] = $data['content'];
        $insert_data['image'] = $data['image'];

        $query = $this->db->insert('articles', $insert_data);
    }
    public function getArticle($sorting, $offset, $start, $searchparam)
    {
        $sql = "SELECT * FROM articles a
        WHERE (a.title LIKE '%" . $searchparam . "%'
        OR a.content LIKE '%" . $searchparam . "%'
        OR a.image LIKE '%" . $searchparam . "%'
        OR a.created_at LIKE '%" . $searchparam . "%')
        ORDER BY  " . $sorting . " LIMIT " . $start . ", " . $offset . " ";
        //echo $sql;
        return ($this->db->query($sql)->result());
    }

    public function countArticle()
    {
        $this->db->select('*');
        $this->db->from('articles');
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function save_update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('articles', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function delete_article($id)
    {
        $this->db->query('DELETE articles FROM articles where id="' . $id . '"');
    }
}
