<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Slider_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //save picture data to db
    public function store_pic_data($data)
    {
        $insert_data['title'] = $data['title'];
        $insert_data['image'] = $data['image'];

        $query = $this->db->insert('sliders', $insert_data);
    }
    public function getSliders($sorting, $offset, $start, $searchparam)
    {

        $sql = "SELECT * FROM sliders
        WHERE (title LIKE '%" . $searchparam . "%'
        OR image LIKE '%" . $searchparam . "%'
        OR created_at LIKE '%" . $searchparam . "%')
        ORDER BY  " . $sorting . " LIMIT " . $start . ", " . $offset . " ";
        //echo $sql;
        return ($this->db->query($sql)->result());
    }

    public function count_slider()
    {
        $this->db->select('*');
        $this->db->from('sliders');
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function delete_slider($id)
    {
        $this->db->query('DELETE sliders FROM sliders where id="' . $id . '"');
    }
}
