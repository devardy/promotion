<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Promotion_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //save picture data to db
    public function store_pic_data($data)
    {
        $insert_data['title'] = $data['title'];
        $insert_data['slug'] = $data['slug'];
        $insert_data['category_id'] = $data['category_id'];
        $insert_data['content'] = $data['content'];
        $insert_data['price'] = $data['price'];
        $insert_data['image'] = $data['image'];

        $query = $this->db->insert('promotions', $insert_data);
    }
    public function getPromotion($sorting, $offset, $start, $searchparam)
    {

        $sql = "SELECT p.*, c.category_name
        FROM promotions p
        LEFT JOIN categories c ON c.id = p.category_id
        WHERE (title LIKE '%" . $searchparam . "%'
        OR c.category_name LIKE '%" . $searchparam . "%'
        OR p.content LIKE '%" . $searchparam . "%'
        OR p.price LIKE '%" . $searchparam . "%'
        OR p.image LIKE '%" . $searchparam . "%'
        OR p.created_at LIKE '%" . $searchparam . "%')
        ORDER BY  " . $sorting . " LIMIT " . $start . ", " . $offset . " ";
        //echo $sql;
        return ($this->db->query($sql)->result());
    }

    public function count_promotion()
    {
        $this->db->select('p.*, c.category_name');
        $this->db->from('promotions p');
        $this->db->join('categories c', 'c.id = p.category_id', 'left');
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function delete_promotion($id)
    {
        $this->db->query('DELETE promotions FROM promotions where id="' . $id . '"');
    }
    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('promotions', $data);
        $this->db->order_by('created_at', 'DESC');
    }

    //update image promotion
    public function save_update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('promotions', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_category($data)
    {
        $this->db->insert('categories', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getCategory($sorting, $offset, $start, $searchparam)
    {

        $sql = "SELECT * FROM categories
        WHERE (category_name LIKE '%" . $searchparam . "%'
        OR created_at LIKE '%" . $searchparam . "%')
        ORDER BY  " . $sorting . " LIMIT " . $start . ", " . $offset . " ";
        //echo $sql;
        return ($this->db->query($sql)->result());
    }

    public function count_category()
    {
        $this->db->select('*');
        $this->db->from('categories');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function updateCategory($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('categories', $data);
        $this->db->order_by('created_at', 'DESC');
    }

    public function deleteCategory($id)
    {
        $this->db->query('DELETE categories FROM categories where id="' . $id . '"');
    }

    public function seed_promotion($data)
    {
        $this->db->insert('promotions', $data);
        return $this->db->insert_id();
    }
}
