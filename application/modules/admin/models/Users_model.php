<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Users_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add_admin($data)
    {
        $this->db->insert('admin', $data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function getAdminList($sorting, $offset, $start, $searchparam)
    {

        $sql = "SELECT * FROM `admin`
        WHERE (fullname LIKE '%" . $searchparam . "%'
        OR username LIKE '%" . $searchparam . "%'
        OR created_at LIKE '%" . $searchparam . "%')
        ORDER BY  " . $sorting . " LIMIT " . $start . ", " . $offset . " ";
        //echo $sql;
        return ($this->db->query($sql)->result());
    }

    public function count_admin()
    {
        $this->db->select('*');
        $this->db->from('admin');
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function update_admin($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('admin', $data);
        $this->db->order_by('created_at', 'DESC');
    }
}
