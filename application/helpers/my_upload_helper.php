<?php
function handel_upload_article($id)
{
    if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {

        $CI = &get_instance();
        $CI->load->database();
        $config['upload_path'] = ARTICLE_IMAGE;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '2000';

        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);

        if (!$CI->upload->do_upload('image')) {
            $error = $CI->upload->display_errors();
            $CI->message->custom_error_msg('admin/article/index', $error);
            return false;
            // uploading failed. $error will holds the errors.
        } else {
            $fdata = $CI->upload->data();
            $CI->db->where('id', $id);
            $CI->db->update('articles', array(
                'image' => $fdata['file_name']
            ));

            // uploading successfull, now do your further actions
        }
    }
    return false;
}
