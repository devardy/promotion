var apis = {
  add_admin: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "add_addmin",
      data: data,
    });
  },
  delete_admin: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "admin_delete",
      data: data,
    });
  },
  deleteSlider: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "delete",
      data: data,
    });
  },
  deletePromotion: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "promotion_delete",
      data: data,
    });
  },
  add_category: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "add-category",
      data: data,
    });
  },
  deleteCategory: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "category-delete",
      data: data,
    });
  },
  deleteArticle: function (data) {
    return $.ajax({
      type: "POST",
      url: base_url + "delete-article",
      data: data,
    });
  },
};
